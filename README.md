#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <cmath>

using namespace std;

void swap(unsigned char* a, unsigned char* b)  //перестановка значений
{
    unsigned char c = *a;
    *a = *b;
    *b = c;
}

unsigned char* KSA(int n)
{
    vector<char> Key;
    int size = -1;
    FILE* KeyFile;
    char FName[] = { "Key.txt" };
    KeyFile = fopen(FName, "r");    //файл, хранящий ключ
    unsigned char symbol;
    while (!feof(KeyFile))          //считываем ключ
    {
        symbol = getc(KeyFile);
        size++;
        Key.push_back(symbol);
        //cout << "KSA" << endl;
    }
    Key.pop_back();
    fclose(KeyFile);
    cout << "Your key: ";
    for (int i = 0; i < size; i++)
    {
        cout << Key[i] << ", ";
    }
    cout << endl;
    unsigned char* S = new unsigned char[int(pow(2, n))];    //инициализация S-блока
    for (int i = 0; i < (int(pow(2, n))); i++)
        S[i] = i;
    int j = 0;
    for (int i = 0; i < (int(pow(2, n))); i++)
    {
        j = ((j + S[i] + Key[i % size]) % (int(pow(2, n))));
        swap(&S[i], &S[j]);
    }
    cout << "S-block:" << endl;
    for (int i = 0; i < (int(pow(2, n))); i++)
        cout << "S[" << i << "]" << (int(S[i])) << endl;
    cout << endl;
    return S;
    delete[] S;
}

void Encrypt(int n)            //шифрование
{
    FILE* TxtFile, * EncrFile;
    char TxtFName[] = { "TxtFile.txt" };
    char EncrFName[] = { "EncrFile.txt" };
    TxtFile = fopen(TxtFName, "r");
    EncrFile = fopen(EncrFName, "w");
    unsigned char* S = new unsigned char[int(pow(2, n))];
    int i = 0, j = 0;
    unsigned char EncrSymb = 0;   
    char m;
    cout << "Encryption process: " << endl;
    S = KSA(n);                //инициализация S-блока
    while (!feof(TxtFile))     //генерация псевдослучайного слова
    {
        i = ((i + 1) % (int(pow(2, n))));
        j = ((j + S[i]) % (int(pow(2, n))));
        unsigned char t = S[(S[i] + S[j]) % (int(pow(2, n)))];           //генерация псевдослучайного слова t
        m = EncrSymb ^ t;                                               //элемент m c индексом t
        fprintf(EncrFile, "%c", EncrSymb);
        cout << int(m) << ", ";
        EncrSymb = getc(TxtFile);
        //cout << "PRGA" << endl;
    }
    cout << endl << "Encryption process is ended." << endl;
    fclose(TxtFile);
    fclose(EncrFile);
    delete[] S;
}

void Decrypt(int n)              //дешифровка
{
    FILE* EncrFile, * DecrFile;
    char DecrFName[] = { "DecrFile.txt" };
    char EncrFName[] = { "EncrFile.txt" };
    DecrFile = fopen(DecrFName, "w");
    EncrFile = fopen(EncrFName, "r");
    unsigned char* S = new unsigned char[int(pow(2, n))];
    unsigned char EncrSymb = 0;
    char m; 
    int i = 0, j = 0;
    cout << "Decryption process: " << endl;
    S = KSA(n);                 //инициализация S-блока
    while (!feof(EncrFile))         //prga
    {
        i = ((i + 1) % (int(pow(2, n))));
        j = ((j + S[i]) % (int(pow(2, n))));
        unsigned char t = S[(S[i] + S[j]) % (int(pow(2, n)))];        //генерация псевдослучайного слова t
        m = EncrSymb ^ t;                                            //элемент m c индексом t
        fprintf(DecrFile, "%c", EncrSymb);
        cout << int(m) << ", ";
        EncrSymb = getc(EncrFile);
    }
    cout << endl << "Decryption process is ended." << endl;
    fclose(DecrFile);
    fclose(EncrFile);
    delete[] S;
}

int main()
{
    system("color F0");
    int n = 0;
    cout << "Please, enter the size of block  n : ";
    cin >> n;
    cout << endl;
    Encrypt(n);
    Decrypt(n);
    system("pause");
    return 0;
}